import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'my-collection', pathMatch: 'full' },
  { path: 'my-collection', loadChildren: './pages/my-collection/my-collection.module#MyCollectionPageModule' },
  { path: 'search', loadChildren: './pages/search/search.module#SearchPageModule' },
  { path: 'chapter', loadChildren: './pages/chapter/chapter.module#ChapterModule' },
  { path: 'novel', loadChildren: './pages/novel/novel.module#NovelPageModule' },
  { path: '**', redirectTo: 'search' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
