import * as NovelActions from './novel.actions';

export interface State {
  collection: { text: string, link: string }[];
}

export const novelInitialState: State = {
  collection: []
};

export const novelReducer = (
  state: State = novelInitialState,
  action: NovelActions.NovelActionsUnion
): State => {
  switch (action.type) {
    case NovelActions.NovelActionTypes.AddToCollection: {
      const novel = action.payload;

      state.collection.push(novel);

      return state;
    }

    case NovelActions.NovelActionTypes.RemoveFromCollection: {
      const novelText = action.payload.text;

      state.collection = state.collection
        .filter((novel) => novel.text !== novelText);

      return state;
    }

    default: {
      return state;
    }
  }
}
