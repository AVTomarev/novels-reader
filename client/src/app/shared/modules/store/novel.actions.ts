import { Action } from '@ngrx/store';

export enum NovelActionTypes {
  AddToCollection = '[Novel CRUD] Add To Collection',
  RemoveFromCollection = '[Novel CRUD] Remove From Collection'
}

export class AddToCollection implements Action {
  readonly type = NovelActionTypes.AddToCollection;

  constructor(
    public payload: { text: string, link: string }
  ) {}
}

export class RemoveFromCollection implements Action {
  readonly type = NovelActionTypes.RemoveFromCollection;

  constructor(
    public payload: { text: string; }
  ) {}
}

export type NovelActionsUnion = AddToCollection | RemoveFromCollection;
