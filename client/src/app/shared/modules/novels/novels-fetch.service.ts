import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NovelsFetchService {
  constructor(
    private httpClient: HttpClient
  ) {}

  public getChapter(url: string): Observable<{}> {
    return this.httpClient.get('/api/novel/chapter', { params: { url } });
  }

  public getNovel(url: string): Observable<{}> {
    return this.httpClient.get('/api/novel', { params: { url } });
  }

  public getSearchPage(url: string): Observable<{}> {
    return this.httpClient.get('/api/search', { params: { url } });
  }
}
