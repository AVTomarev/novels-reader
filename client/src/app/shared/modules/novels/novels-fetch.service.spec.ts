import { TestBed } from '@angular/core/testing';

import { NovelsFetchService } from './novels-fetch.service';

describe('novelsFetchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NovelsFetchService = TestBed.get(NovelsFetchService);
    expect(service).toBeTruthy();
  });
});
