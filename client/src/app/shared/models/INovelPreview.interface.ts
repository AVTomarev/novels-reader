import { containsValidProps } from './validators/containsValidProps.validator';
import { ILinkSimple, isLinkSimple } from './ILinkSimple.interface';
import { IPropsAndTypes } from './validators/models';

export interface INovelPreview {
  link: string;
  title: string;
  imageSrc: string;
  tags?: ILinkSimple[];
  genres?: ILinkSimple[];
  chaptersNumber?: string;
}

export const isNovelPreview = (object: any): object is INovelPreview => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'link': [ { typeName: 'string' } ],
    'title': [ { typeName: 'string' } ],
    'imageSrc': [ { typeName: 'string' } ],
  };

  return containsValidProps(object, requiredProps);
}
