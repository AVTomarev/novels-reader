export interface ICustomErrorSimple {
	name: string;
	message: string;
	type: string;
}
