import { containsValidProps } from '../validators/containsValidProps.validator';
import { INovel } from '../INovel.interface';

export interface ICollectionPage {
  novelsList: INovel[];
}

export const isCollectionPage = (object: object): object is ICollectionPage => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps = {
  };

  return containsValidProps(object, requiredProps);
}
