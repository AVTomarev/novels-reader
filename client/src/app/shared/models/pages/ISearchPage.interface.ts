import { containsValidProps } from '../validators/containsValidProps.validator';
import { INovelPreview, isNovelPreview } from '../INovelPreview.interface';
import { IPropsAndTypes } from '../validators/models';

export interface ISearchPage {
  translationsFound: string;
  novelsList: INovelPreview[];
}

export const isSearchPage = (object: object): object is ISearchPage => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'translationsFound': [
      {
        typeName: 'string'
      }
    ],
    novelsList: [
      {
        typeName: 'array',
        validateFunction: isNovelPreview
      }
    ]
  };

  return containsValidProps(object, requiredProps);
}
