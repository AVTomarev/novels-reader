import { containsValidProps } from '../validators/containsValidProps.validator';
import { IPropsAndTypes } from '../validators/models';
import { isString } from '../validators/simpleTypeValidators';

export interface IChapterPage {
  title: string;
  link: string;
  content: string[];
  nextChapter?: string;
  prevChapter?: string;
}

export const isChapterPage = (object: any): object is IChapterPage => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'title': [ { typeName: 'string' } ],
    'link': [ { typeName: 'string' } ],
    'content': [
      {
        typeName: 'array',
        validateFunction: isString
      }
    ] 
  };

  return containsValidProps(object, requiredProps);
}
