import { isChapterPreview } from './../IChapterPreview.interface';
import { containsValidProps } from '../validators/containsValidProps.validator';
import { IChapterPreview } from '../IChapterPreview.interface';
import { IPropsAndTypes } from '../validators/models';
import { isString } from '../validators/simpleTypeValidators';

export interface INovelPage {
  link: string;
  title: string;
  images: string[];
  chaptersList: IChapterPreview[];
  author?: string;
  releaseYear?: string;
  numberOfChapters?: string;
  description?: string[];
  reviews?: string[];
}

export const isNovelPage = (object: any): object is INovelPage => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'link': [ { typeName: 'string' } ],
    'title': [ { typeName: 'string' } ],
    'images': [
      {
        typeName: 'array',
        validateFunction: isString
      }
    ],
    'chaptersList': [
      {
        typeName: 'array',
        validateFunction: isChapterPreview
      }
    ]
  }

  return containsValidProps(object, requiredProps);
}
