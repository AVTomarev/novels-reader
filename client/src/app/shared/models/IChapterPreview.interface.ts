import { containsValidProps } from './validators/containsValidProps.validator';
import { IPropsAndTypes } from './validators/models';

export interface IChapterPreview {
  link: string;
  title: string;
  isReadable?: boolean;
  isLoadable?: boolean;
}

export const isChapterPreview = (object: any): object is IChapterPreview => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'link': [ { typeName: 'string' } ],
    'title': [ { typeName: 'string' } ]
  }

  return containsValidProps(object, requiredProps);
}
