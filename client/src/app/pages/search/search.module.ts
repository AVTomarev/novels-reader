import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchPage } from './search.page';

/* MODULES */
import { NovelsModule } from 'src/app/shared/modules/novels/novels.module';
/* MODULES END */

const routes: Routes = [
  {
    path: '',
    component: SearchPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NovelsModule
  ],
  declarations: [SearchPage]
})
export class SearchPageModule {}
