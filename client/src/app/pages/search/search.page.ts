import { Component, OnInit } from '@angular/core';
import { NovelsFetchService } from 'src/app/shared/modules/novels/novels-fetch.service';
import { AlertController, NavController } from '@ionic/angular';

import { INovelPreview } from 'src/app/shared/models/INovelPreview.interface';
import { isSearchPage } from 'src/app/shared/models/pages/ISearchPage.interface';
import { isCustomErrorSimple } from 'src/app/shared/models/custom-errors/CustomErrorSimple.class';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  public list?: INovelPreview[];

  public isContentLoading: boolean = false;

  constructor(
    private novelsFetchService: NovelsFetchService,
    private alertController: AlertController,
    private navController: NavController
  ) {}

  ngOnInit() {
  }

  public handleSitePickerClick(link: string): void {
    this.list = undefined;
    this.isContentLoading = true;

    this.novelsFetchService
      .getSearchPage(link)
      .subscribe(JSON => {
        console.log(JSON);

        if (isSearchPage(JSON)) {
          this.list = JSON.novelsList;
        }
        else if (isCustomErrorSimple(JSON)) {
          console.log('JSON is CustomError!!!');
        
          this.alertController
            .create({
              header: JSON['name'],
              message: JSON['message'],
              buttons: [{
                text: 'Ok',
                handler: () => {
                  this.navController.back();
                }
              }]
            })
            .then(alert => {
              alert.present();
            });
        }
        else {
          console.log('JSON is not Error or SearchPage! \n', JSON);
        }
        
        this.isContentLoading = false;
      });
  }
}
