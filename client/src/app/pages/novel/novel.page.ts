import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NovelsFetchService } from 'src/app/shared/modules/novels/novels-fetch.service';
import { AlertController, NavController } from '@ionic/angular';

import { INovelPage, isNovelPage } from 'src/app/shared/models/pages/INovelPage.interface';
import { isCustomErrorSimple } from 'src/app/shared/models/custom-errors/CustomErrorSimple.class';

@Component({
  selector: 'app-novel',
  templateUrl: './novel.page.html',
  styleUrls: ['./novel.page.scss'],
})
export class NovelPage implements OnInit {
  public novel?: INovelPage;

  public isContentLoading: boolean = false;

  public sliderOptions = {
    initialSlide: 1,
    speed: 400,
    pager: true
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private novelsFetchService: NovelsFetchService,
    private alertController: AlertController,
    private navController: NavController
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const link = params.linkToNovel;
      if (typeof link !== 'string') {
        alert('something went wrong with link to novel');
        return;
      }

      this.getNovel(link);
    });
  }

  private getNovel(link: string): void {
    this.novel = undefined;
    this.isContentLoading = true;

    this.novelsFetchService
      .getNovel(link)
      .subscribe(JSON => {
        console.log(JSON);

        if (isNovelPage(JSON)) {
          this.novel = JSON as INovelPage;
        }
        else if (isCustomErrorSimple(JSON)) {
          console.log('JSON is CustomError!!!');
        
          this.alertController
            .create({
              header: JSON['name'],
              message: JSON['message'],
              buttons: [{
                text: 'Ok',
                handler: () => {
                  this.navController.back();
                }
              }]
            })
            .then(alert => {
              alert.present();
            });
        }
        else {
          console.log('JSON is not Error or NovelPage! \n', JSON);
        }

        this.isContentLoading = false;
      });
  }
}
