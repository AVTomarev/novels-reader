import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { NovelPage } from './novel.page';

import { NovelsModule } from 'src/app/shared/modules/novels/novels.module';

const routes: Routes = [
  {
    path: '',
    component: NovelPage
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NovelsModule
  ],
  declarations: [
    NovelPage
  ]
})
export class NovelPageModule {}
