import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reader-mode-toolbar',
  templateUrl: './reader-mode-toolbar.component.html',
  styleUrls: ['./reader-mode-toolbar.component.scss'],
})
export class ReaderModeToolbarComponent implements OnInit {

  @Input() prevLink?: string;

  @Input() nextLink?: string;

  constructor() { }

  ngOnInit() {}
}
