import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonContent, AlertController, NavController } from '@ionic/angular';

import { NovelsFetchService } from 'src/app/shared/modules/novels/novels-fetch.service';

import { IChapterPage, isChapterPage } from 'src/app/shared/models/pages/IChapterPage.interface';
import { isCustomErrorSimple } from 'src/app/shared/models/custom-errors/CustomErrorSimple.class';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.page.html',
  styleUrls: ['./chapter.page.scss'],
})
export class ChapterPage implements OnInit {
  public currentChapter?: IChapterPage;

  @ViewChild(IonContent, { static: true })
  content!: IonContent;

  public percentOfToolbarHides: number = 0;

  private lastScrollXPoint: number = 0;

  constructor(
    private novelsFetchService: NovelsFetchService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    private navController: NavController
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.getChapter(params.url);
    });
  }

  private getChapter(url: string): void {
    this.currentChapter = undefined;

    this.novelsFetchService
      .getChapter(url)
      .subscribe(JSON => {
        console.log(JSON);

        if (isChapterPage(JSON)) {
          this.currentChapter = JSON;
          this.content.scrollToTop();
        }
        else if (isCustomErrorSimple(JSON)) {
          console.log('JSON is CustomError!!!');
        
          this.alertController
            .create({
              header: JSON['name'],
              message: JSON['message'],
              buttons: [{
                text: 'Ok',
                handler: () => {
                  this.navController.back();
                }
              }]
            })
            .then(alert => {
              alert.present();
            });
        }
        else {
          console.log('JSON is not Error or ChapterPage! \n', JSON);
        }
    });
  }

  public handleScrollEvent(event: CustomEvent): void {
    const scrollToTop = event.detail.scrollTop;
    if (typeof scrollToTop === 'number') {
      this.changePercentageOfToolbarHides(scrollToTop);
    }
  }

  private changePercentageOfToolbarHides(currentPoint: number): void {
    const lastPoint = this.lastScrollXPoint;
    const currentPercent = this.percentOfToolbarHides;

    if (lastPoint < currentPoint) {
      const percent = (currentPoint - lastPoint) + currentPercent;

      this.percentOfToolbarHides = percent >= 100 ? 100 : percent;
    } else if (lastPoint > currentPoint) {
      const percent = currentPercent - (lastPoint - currentPoint);

      this.percentOfToolbarHides = percent <= 0 ? 0 : percent;
    }

    this.lastScrollXPoint = currentPoint;
  }
}
