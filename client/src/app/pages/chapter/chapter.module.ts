import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { NovelsModule } from 'src/app/shared/modules/novels/novels.module';

import { ChapterPage } from './chapter.page';
import { ReaderModeToolbarComponent } from './components/reader-mode-toolbar/reader-mode-toolbar.component';

const routes: Routes = [
  {
    path: '',
    component: ChapterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NovelsModule
  ],
  declarations: [
    ChapterPage,
    ReaderModeToolbarComponent
  ]
})
export class ChapterModule {}
