### If TypeScript is not installed on your computer
```bash
npm i -g typescript
```

### If Ionic CLI is not installed on your computer
```bash
npm i -g ionic
```

### Install modules for project
```bash
npm i
npm run install-client-server
```

### For serve client and server
```bash
npm run start
```