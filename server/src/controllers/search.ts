import {
  Request,
  Response
} from 'express';
import puppeteer from 'puppeteer';

import * as pagesAssembler from '../modules/pagesAssembler/pagesAssembler';
import { pagesAssemblySchemas } from '../modules/pagesAssembler/schemas';

import { isSearchPage } from '../models/pages/ISearchPage.interface';
import { ValidationError } from '../models/custom-errors/ValidationError.class';

export const getPage = async (req: Request, res: Response) => {
  const url = req.query.url;
  if (typeof url !== 'string') {
    res.status(452).json({
      header: 'Parameter request.query.url does not math "string" type'
    });
    return;
  }

  let siteName: RegExpMatchArray | null | string =
    url.match(/https:\/\/.*\.ru/);
    
  if (siteName === null) {
    res.status(452).json({
      header: 'Cannot find siteName in url' + url
    });
    return;
  } else {
    siteName = siteName[0];
  }

  const options = {
    uri: url,
    transform: (body: any) => body
  }
  
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(options.uri);
  let content = await page.content();

  const pageAssemblySchema = pagesAssemblySchemas[siteName].searchPage;
  
  const newPage = pagesAssembler.createNewPage(
    content,
    pageAssemblySchema
  );

  if (isSearchPage(newPage)) {
    res.json(newPage);
  } else {
    const err = new ValidationError('Parsed page content is not passed validation').getError();

    res.json(err);
  }
}
