import {
	Request,
	Response
} from 'express';
import requestPromise from 'request-promise';

import * as pagesAssembler from '../modules/pagesAssembler/pagesAssembler';
import { pagesAssemblySchemas } from '../modules/pagesAssembler/schemas';

import { ValidationError } from '../models/custom-errors/ValidationError.class';
import { CustomErrorSimple } from '../models/custom-errors/CustomErrorSimple.class';
import { isChapterPage } from '../models/pages/IChapterPage.interface';

export const getChapter = async (req: Request, res: Response) => {
	const url = req.query.url;
	if (typeof url !== 'string') {
		res.status(452).json({
			header: 'Parameter request.query.url does not math "string" type'
		});
		return;
	}

	const options = {
		uri: url,
		transform: (body: any) => body
	}

	requestPromise(options)
		.then((content) => {
			let siteName: RegExpMatchArray | null | string =
				url.match(/https:\/\/.*\.ru/);

			if (siteName === null) {
				res.status(452).json({
					header: 'Cannot find siteName in url' + url
				});
				return;
			} else {
				siteName = siteName[0];
			}

			const pageAssemblySchema = pagesAssemblySchemas[siteName].chapter;

			const newPage = pagesAssembler.createNewPage(
				content,
				pageAssemblySchema
			);

			// Adding property link because we can't get it from page parsing
			newPage.link = url;

			if (isChapterPage(newPage)) {
				res.json(newPage);
			} else {
				const err = new ValidationError('Parsed page content is not passed validation').getError();

				res.json(err);
			}
		})
		.catch((err) => {
			res.json({
				header: 'Something went wrong',
				errorMsg: err.message,
				errorHead: err.name
			});
		});
}
