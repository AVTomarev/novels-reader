import { containsValidProps } from './validators/containsValidProps.validator';
import { IPropsAndTypes } from './validators/models';

export interface IChapter {
  id: string;
  title: string;
  content: string[];
  nextChapter?: string;
  prevChapter?: string;
  progress?: number;
}

export const isChapter = (object: any): object is IChapter => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'id': [ { typeName: 'string' } ],
    'title': [ { typeName: 'string' } ],
    'content': [ { typeName: 'string' } ]
  };

  return containsValidProps(object, requiredProps);
}
