import { containsValidProps } from './validators/containsValidProps.validator';
import { IPropsAndTypes } from './validators/models';

export interface ILinkSimple {
  text: string;
  link: string;
}

export const isLinkSimple = (object: any): object is ILinkSimple => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'text': [ { typeName: 'string' } ],
    'link': [ { typeName: 'string' } ]
  }

  return containsValidProps(object, requiredProps);
}
