import { containsValidProps } from './validators/containsValidProps.validator';
import { IChapter } from './IChapter.interface';
import { IPropsAndTypes } from './validators/models';

export interface INovel {
  id: string;
  title: string;
  imageSrc: string;
  lastOpenedChapterId?: string;
  loadedChapters?: IChapter[];
}

export const isNovel = (object: any): object is INovel => {
  if (typeof object !== 'object' || Array.isArray(object)) { return false; }

  const requiredProps: IPropsAndTypes = {
    'id': [ { typeName: 'string' } ],
    'title': [ { typeName: 'string' } ],
    'imageSrc': [ { typeName: 'string' } ]
  };

  return containsValidProps(object, requiredProps);
}
