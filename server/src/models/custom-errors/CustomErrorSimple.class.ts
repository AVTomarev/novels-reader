import { ICustomErrorSimple } from './ICustomErrorSimple.interface';
import { containsValidProps } from '../validators/containsValidProps.validator';
import { IPropsAndTypes } from '../validators/models';

export class CustomErrorSimple {
    
    private name: string;

    private message: string;

    private type: string;

    constructor(
        message: string,
        name: string = 'Custom Error'
    ) {
        const type: string = 'error';

        this.message = message;
        this.name = name;
        this.type = type;
    }

    public getError(): ICustomErrorSimple {
        return {
            name: this.name,
            message: this.message,
            type: this.type
        }
    }
}

export const isCustomErrorSimple = (obj: any): obj is CustomErrorSimple => {
    if (typeof obj !== 'object' || Array.isArray(obj)) { return false; }

    const requiredProps: IPropsAndTypes = {
        'message': [ { typeName: 'string' } ],
        'name': [ { typeName: 'string' } ],
        'type': [ { typeName: 'string' } ]
    }
    
    return containsValidProps(obj, requiredProps) && obj['type'] === 'error';
}
