import { CustomErrorSimple } from './CustomErrorSimple.class';

export class ValidationError extends CustomErrorSimple {
    constructor(
        message: string
    ) {
        const name = 'Validation Error';

        super(message, name);
    }
}
