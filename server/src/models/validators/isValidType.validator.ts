export const isValidType = <T>(
  value: T,
  type: string
): boolean => {
  return typeof value === type;
}
