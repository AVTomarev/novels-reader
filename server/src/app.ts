// Import modules from node_modules
import express, { Application } from 'express';

// Import controllers(route handlers)
import * as chapterController from './controllers/chapter';
import * as novelController from './controllers/novel';
import * as search from './controllers/search';

// Create Express server
const app: Application = express();

// Express config
app.set('port', process.env.port || 3000);

// App routes
app.get('/api/search', search.getPage);
app.get('/api/novel/chapter', chapterController.getChapter);
app.get('/api/novel', novelController.getNovel);

export default app;
