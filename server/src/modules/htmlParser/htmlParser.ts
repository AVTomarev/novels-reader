import cheerio from 'cheerio';
import { ISearchedPartOfPageContent } from './models';

interface IParsedSelector {
	selector: string,
	filter?: string
}

export const parseHtml = (
	body: string,
	options: ISearchedPartOfPageContent
): string | string[] | undefined => {
	const $ = cheerio.load(body);
	const {
		selector,
		context = '',
		root = '',
		attrName,
		allFound
	} = options;
	
	let result;

	/* searching elements */
	result = $(selector, context, root);
	if (!result) { return undefined; }

	/* getting needed attr or text */
	result = result.map((i, item) => {
		let value = attrName ?
			$(item).attr(attrName) :
			$(item).text();

		if (value !== '') {
			return value;
		}
	}).get();

	if (allFound && result.length > 0) {
			return result;
	} else if (result.length > 0) {
			return result[0];
	}
	
	return undefined;
}

export const getHTMLElements = (
	body: string,
	selector: string
): string[] => {
	const parsedSelectors = parseSelector(selector);

	const selectedItems = parsedSelectors.reduce(
		(accum: string[], selector, i) => {
			let result = (i === 0) ?
				getElements(body, selector) :
				accum.reduce(
					(contexts: string[], context) => {
						return contexts.concat( getElements(context, selector) );
					}, []);

			return result;
		}, []);

	return selectedItems;
}

function getElements(
	body: string,
	option: IParsedSelector
): string[] {
	const $ = cheerio.load(body);

	let result = $(option.selector)
		.filter((i, element) => {
			const elem = $(element).text();
			if (elem === null) { return false; }

			return option.filter ?
				elem.includes(option.filter) :
				true;
		})
		.get();

	return result;
}

function parseSelector(string: string): IParsedSelector[] {
	const splittedString = string.split(/(?<=.*@.*@)\s/g);
	
	return splittedString.map((str) => {
		const filterMatch = str.match(/(?<=@).*(?=@)/);
		const filter = filterMatch === null ?
			undefined :
			filterMatch[0];

		const selectorMatch = str.match(/^.*(?=@.*@)|^.*$/);
		const selector = selectorMatch === null ?
			'' :
			selectorMatch[0];

		return filter ? { selector, filter } : { selector }
	});
}
