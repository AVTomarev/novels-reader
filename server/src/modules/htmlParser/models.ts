export interface ISearchedPartOfPageContent {
	selector: string;
	context?: string;
	root?: string;
	attrName?: string;
	allFound?: boolean;
}
