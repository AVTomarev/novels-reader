import * as htmlParser from '../htmlParser/htmlParser';

import {
  IAssemblySchemasItemsContext,
  IAssemblySchemasItem,
  assemblySchemasType,
  IItemFormattingOptions,
  ITextReplacer,
  IPageResult
} from './models';

export const createNewPage = (
  body: string,
  assemblySchema: Array<IAssemblySchemasItemsContext | IAssemblySchemasItem>
): IPageResult => {
  
  const result = assemblySchema.reduce((
    accum: IPageResult,
    item: IAssemblySchemasItemsContext | IAssemblySchemasItem
  ) => {

    if (item.type === assemblySchemasType.item) {
      
      let parsedItem = htmlParser.parseHtml(body, item.itemSelect);
      if (!parsedItem) { return accum; }

      if (item.itemFormatting) {
        parsedItem = formatItem(parsedItem, item.itemFormatting)
      }
      
      accum[item.name] = parsedItem;
    } else {

      const context = htmlParser.getHTMLElements(body, item.scopeSelectors.selector);
    
      let result: IPageResult[] | string[] = context;

      if (item.scopeSelectors.filter) {
        result = filterStrings(result, item.scopeSelectors.filter);
      }

      result = result.map((element) => {
        return createNewPage(element, item.props);
      });

      accum[item.name] = result;
    }

    return accum;
  }, {});

  return result;
}

function formatItem(
  value: string | string[],
  formatOptions: IItemFormattingOptions
): string | string[] {

  let item = value;

  if (formatOptions.filter && typeof item !== 'string') {
    item = filterStrings(item, formatOptions.filter);
  }

  if (formatOptions.replacingText) {
    item = (typeof item === 'string') ?
      cutString(item, formatOptions.replacingText) :
      item.map((elem) => {
        if (!formatOptions.replacingText) { return elem; }

        return cutString(elem, formatOptions.replacingText);
      });
  }

  return item;
}

function cutString(
  str: string,
  options: ITextReplacer[]
): string {

  return options.reduce((result, option) => {
    const {
      target,
      replacer = ''
    } = option;
    
    return result.replace(target, replacer)
  }, str);
}

function filterStrings(
  strings: string[],
  filter: Array<string | RegExp>
): string[] {

  return filter.reduce((res: string[], filterItem) => {
    
    return res.filter((str) => {
      return str.match(filterItem);
    });

  }, strings);
}
