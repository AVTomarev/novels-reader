import {
  IPagesAssemblySchemas,
  assemblySchemasType
} from './models';

export const pagesAssemblySchemas: IPagesAssemblySchemas = {
  'https://tl.rulate.ru': {
    searchPage: [
      {
        name: 'translationsFound',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'h3'
        }
      },
      {
        name: 'novelsList',
        type: assemblySchemasType.itemsContext,
        scopeSelectors: {
          selector: 'ul.search-results > li',
          allFound: true
        },
        props: [
          {
            name: 'imageSrc',
            type: assemblySchemasType.item,
            itemSelect: {
              selector: 'div.th > img',
              attrName: 'src',
            },
            itemFormatting: {
              replacingText: [
                {
                  target: /^/,
                  replacer: 'https://tl.rulate.ru'
                }
              ]
            }
          },
          {
            name: 'title',
            type: assemblySchemasType.item,
            itemSelect: {
              selector: 'p.book-tooltip > a'
            }
          },
          {
            name: 'link',
            type: assemblySchemasType.item,
            itemSelect: {
              selector: 'p.book-tooltip > a',
              attrName: 'href',
            },
            itemFormatting: {
              replacingText: [
                {
                  target: /^/,
                  replacer: 'https://tl.rulate.ru'
                }
              ]
            }
          },
          {
            name: 'genres',
            type: assemblySchemasType.itemsContext,
            scopeSelectors: {
              selector: 'div.meta > p @жанры:@ a',
            },
            props: [
              {
                name: 'link',
                type: assemblySchemasType.item,
                itemSelect: {
                  selector: ':root',
                  attrName: 'href'
                },
                itemFormatting: {
                  replacingText: [
                    {
                      target: /^/,
                      replacer: 'https://tl.rulate.ru'
                    }
                  ]
                }
              },
              {
                name: 'text',
                type: assemblySchemasType.item,
                itemSelect: {
                  selector: ':root'
                }
              }
            ]
          },
          {
            name: 'tags',
            type: assemblySchemasType.itemsContext,
            scopeSelectors: {
              selector: 'div.meta > p @тэги:@ a',
            },
            props: [
              {
                name: 'link',
                type: assemblySchemasType.item,
                itemSelect: {
                  selector: ':root',
                  attrName: 'href'
                },
                itemFormatting: {
                  replacingText: [
                    {
                      target: /^/,
                      replacer: 'https://tl.rulate.ru'
                    }
                  ]
                }
              },
              {
                name: 'text',
                type: assemblySchemasType.item,
                itemSelect: {
                  selector: ':root'
                }
              }
            ]
          }
        ]
      }
    ],
    novel: [
      {
        name: 'title',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'h1'
        }
      },
      {
        name: 'images',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'img',
          context: 'div.slick',
          attrName: 'src',
          allFound: true
        },
        itemFormatting: {
          replacingText: [
            {
              target: /^/,
              replacer: 'https://tl.rulate.ru'
            }
          ]
        }
      },
      {
        /* Нужно доработать чтобы получить все элементы!!!! */
        name: 'description',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'div.btn-toolbar + p',
          allFound: true
        }
      },
      {
        /* Нужно доработать чтобы получить все элементы!!!! */
        name: 'reviews',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'div.post div.body',
          allFound: true,
        },
        itemFormatting: {
          // replacingText: [
          //   {
          //     target: /Написал.*[0-9]{1,2}:[0-9]{1,2}/
          //   }
          // ]
        }
      },
      {
        name: 'chaptersList',
        type: assemblySchemasType.itemsContext,
        scopeSelectors: {
          selector: 'tr.chapter_row',
          allFound: true
        },
        props: [
          {
            name: 'title',
            type: assemblySchemasType.item,
            itemSelect: {
              selector: 'td.t > a'
            }
          },
          {
            name: 'link',
            type: assemblySchemasType.item,
            itemSelect: {
              selector: 'td.t > a',
              attrName: 'href'
            },
            itemFormatting: {
              replacingText: [
                {
                  target: /^/,
                  replacer: 'https://tl.rulate.ru'
                }
              ]
            }
          },
          {
            name: 'isReadable',
            type: assemblySchemasType.item,
            itemSelect: {
              selector: 'td.r + td > a'
            }
          }
        ]
      }
    ],
    chapter: [
      {
        name: 'title',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'h1'
        },
        itemFormatting: {
          replacingText: [
            {
              target: 'Готовый перевод '
            }
          ]
        }
      },
      {
        name: 'nextChapter',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'div.span8 div[style] div[style] a:not([href$=ready]) + a',
          attrName: 'href',
        },
        itemFormatting: {
          replacingText: [
            {
              target: /^/,
              replacer: 'https://tl.rulate.ru'
            }
          ]
        }
      },
      {
        name: 'prevChapter',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'div.span8 div div small + a',
          attrName: 'href',
        },
        itemFormatting: {
          replacingText: [
            {
              target: /^/,
              replacer: 'https://tl.rulate.ru'
            }
          ]
        }
      },
      {
        name: 'linkToNovel',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: 'nav > div.navbar-left > a:nth-Child(2)',
          attrName: 'href'
        },
        itemFormatting: {
          replacingText: [
            {
              target: /^/,
              replacer: 'https://tl.rulate.ru'
            }
          ]
        }
      },
      {
        name: 'content',
        type: assemblySchemasType.item,
        itemSelect: {
          selector: '.content-text > p',
          allFound: true
        }
      }
    ]
  }
};
