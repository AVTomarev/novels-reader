import { ISearchedPartOfPageContent } from '../htmlParser/models';

export enum assemblySchemasType {
  itemsContext = 'itemsContext',
  item = 'item'
};

export interface ITextReplacer {
  target: string | RegExp;
  replacer?: string;
}

export interface IItemFormattingOptions {
  filter?: Array<string | RegExp>;
  replacingText?: ITextReplacer[];
}


export interface IAssemblySchemasItem {
  name: string;
  type: assemblySchemasType.item;
  itemSelect: ISearchedPartOfPageContent,
  itemFormatting?: IItemFormattingOptions;
}

export interface IAssemblySchemasItemsContext {
  name: string;
  type: assemblySchemasType.itemsContext;
  scopeSelectors: {
    selector: string;
    context?: string;
    root?: string;
    allFound?: boolean;
    filter?: Array<string | RegExp>;
  },
  props: Array<IAssemblySchemasItemsContext | IAssemblySchemasItem>;
}

export interface IPageAssemblySchema {
  [pageName: string]: Array<IAssemblySchemasItemsContext | IAssemblySchemasItem>
}

export interface IPagesAssemblySchemas {
  [key: string]: IPageAssemblySchema;
}

export interface IPageResult {
  [prop: string]: string | IPageResult | string[] | IPageResult[] | undefined;
}
